package com.company.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Utilities {

    public static List<String> checkFiles(String a){
        try (Stream<Path> stream = Files.list(Paths.get(a))) {
            return stream.flatMap(n ->{
                if (n.toFile().isDirectory()){
                    List<String> folders = checkFiles(n.toString());
                    folders.add("folder");
                    return folders.stream();
                }else {
                    String name = n.getFileName().toString();
                    return Arrays.asList(name.substring(name.lastIndexOf(".") + 1)).stream();
                }
            } ).collect(Collectors.toList());
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }

    public static Map<String,Long> countFolderAndFiles(String a){
        if (a!= null){
            return checkFiles(a)
                    .stream()
                    .collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
        }else return null;

    }

}
